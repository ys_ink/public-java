package com.inkcode;

public class TestClassDefault implements TestClass {
	private String someString;

	/* (non-Javadoc)
	 * @see com.inkcode.TestClass#getSomeString()
	 */
	public String getSomeString() {
		return someString;
	}

	/* (non-Javadoc)
	 * @see com.inkcode.TestClass#setSomeString(java.lang.String)
	 */
	public void setSomeString(String someString) {
		this.someString = someString;
	}
}
