package com.inkcode;

import org.apache.commons.lang.builder.HashCodeBuilder;

public class TestClassHashCodeOnly implements TestClass {
	private String someString;

	public String getSomeString() {
		return someString;
	}

	public void setSomeString(String someString) {
		this.someString = someString;
	}
	
	@Override
	public int hashCode() {
		return new HashCodeBuilder(13, 7)
		.append(someString)
		.toHashCode();
	}
}
