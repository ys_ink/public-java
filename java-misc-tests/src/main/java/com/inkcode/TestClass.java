package com.inkcode;

public interface TestClass {

	public abstract String getSomeString();

	public abstract void setSomeString(String someString);

}