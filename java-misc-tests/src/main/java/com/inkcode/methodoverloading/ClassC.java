package com.inkcode.methodoverloading;

public class ClassC implements InterfaceI{

	public boolean foo(Object f) {
		return false;
	}
	
	public boolean foo(String f) {
		return true;
	}

}
