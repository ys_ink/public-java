package com.inkcode;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

public class TestClassHashCodeEquals implements TestClass {
	private String someString;

	public String getSomeString() {
		return someString;
	}

	public void setSomeString(String someString) {
		this.someString = someString;
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(13, 7).append(someString).hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (obj.getClass() != getClass()) {
			return false;
		}
		TestClassHashCodeEquals rhs = (TestClassHashCodeEquals) obj;
		return new EqualsBuilder()
			.appendSuper(super.equals(obj))
			.append(someString, rhs.getSomeString())
			.isEquals();
	}
}
