package com.inkcode;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

public class HashMapTest {

	
	private static final String SOME_OTHER_STRING = "someOtherString";

	private static final String SOME_STRING = "SomeString";
	
	private static final String WORKS = "works!";

	@Test
	public void testDefaultClass() {
		TestClass obj1 = new TestClassDefault();
		testFunction(obj1);
	}
	
	@Test
	public void testHashCodeOnlyClass() {
		TestClass obj1 = new TestClassHashCodeOnly();
		testFunction(obj1);
	}

	@Test
	public void testHashCodeEqualsClass() {
		TestClass obj1 = new TestClassHashCodeEquals();
		testFunction(obj1);
	}

	private void testFunction(TestClass obj1) {
		obj1.setSomeString(SOME_STRING);
		Map<TestClass, String> map = new HashMap<TestClass, String>();
		
		map.put(obj1, WORKS);
		
		obj1.setSomeString(SOME_OTHER_STRING);
		
		assertTrue(map.containsKey(obj1));
		assertNotNull(map.get(obj1));
		assertEquals(WORKS, map.get(obj1));
		assertEquals(SOME_OTHER_STRING, obj1.getSomeString());
	}
}
