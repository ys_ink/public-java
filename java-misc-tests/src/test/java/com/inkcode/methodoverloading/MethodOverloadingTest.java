package com.inkcode.methodoverloading;

import org.junit.Test;
import static org.junit.Assert.*;
public class MethodOverloadingTest {
	
	@Test
	public void testImpl() {
		ClassC impl = new ClassC();
		assertFalse(impl.foo(new Integer(0)));
		assertTrue(impl.foo("Bla"));
	}
	
	@Test
	public void testInterface() {
		InterfaceI obj = new ClassC();
		assertFalse(obj.foo("Bla"));
	}
}
