package com.inkcode;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

@Named
@SessionScoped
public class LoginController implements Serializable {
    private static final long serialVersionUID = 1L;

    public static final String SUCCESS_MESSAGE = "Welcome";
    public static final String FAILURE_MESSAGE =
        "Incorrect username and password combination";

    @Inject
    private Credentials credentials;
    
    public String login() {
        if ("ink".equals(credentials.getUsername()) &&
            "test".equals(credentials.getPassword())) {
            FacesContext.getCurrentInstance().addMessage(null,
                new FacesMessage(SUCCESS_MESSAGE));
            return "index.xhtml";
        }

        FacesContext.getCurrentInstance().addMessage(null,
            new FacesMessage(FacesMessage.SEVERITY_WARN,
                FAILURE_MESSAGE, FAILURE_MESSAGE));
        return null;
    }
}