package com.inkcode;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

import org.jboss.shrinkwrap.api.Filters;
import org.jboss.shrinkwrap.api.GenericArchive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.asset.StringAsset;
import org.jboss.shrinkwrap.api.importer.ExplodedImporter;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.ConfigurableMavenResolverSystem;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.jboss.shrinkwrap.resolver.api.maven.MavenResolverSystem;
import org.junit.Assert;

public class Deployments {
	private static final String VERSION = ":0.0.1-SNAPSHOT";
	private static final String GROUPID = "com.inkcode.functionaltests:";
	private static final String WEBAPP_SRC = "src/main/webapp";

	public static WebArchive createLoginScreenDeployment() {
		MavenResolverSystem resolver = Maven.resolver();
		resolver.loadPomFromFile("pom.xml");

		String customRepositoryPath = getCustomRepositoryPathFromSurefireProperties();

		WebArchive war = ShrinkWrap.create(WebArchive.class, "fat.war")
				.addAsLibraries(resolveSingleDependency(GROUPID + "functional-tests-demo-web-components" + VERSION, customRepositoryPath))
				.addAsLibraries(resolveSingleDependency(GROUPID + "functional-tests-demo-services" + VERSION, customRepositoryPath))
				.addAsWebInfResource(new File("src/test/resources/web.xml"))
				.addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml")
				.addAsWebInfResource(new StringAsset("<faces-config version=\"2.0\"/>"), "faces-config.xml");
		return war;
	}

	private static String getCustomRepositoryPathFromSurefireProperties() {
		return System.getProperties().getProperty("maven.localRepository");
	}

	private static File resolveSingleDependency(String qualifiedNamed, String customRepositoryPath) {
		File[] files = resolveDependency(qualifiedNamed, customRepositoryPath);
		
		for(File f : files)
		{
			System.out.println(f.toString());
		}
		
		Assert.assertEquals("No library file found for maven artifact: " + qualifiedNamed, 1, files.length);
		return files[0];
	}

	private static File[] resolveDependency(String qualifiedNamed, String customRepositoryPath) {
		ConfigurableMavenResolverSystem resolver;
		resolver = Maven.configureResolver();
		if (customRepositoryPath != null) {
			URL customURL = null;
			try {
				customURL = new File(customRepositoryPath).toURI().toURL();
				System.out.println("customURL: "+customURL);
			} catch (MalformedURLException e) {
				e.printStackTrace();
				Assert.assertTrue(true);
			}
			resolver.withRemoteRepo("local-repo", customURL, "default");
		}
		System.out.println("qualifiedNamed: "+qualifiedNamed);
		File[] files = resolver.resolve(qualifiedNamed).withoutTransitivity().asFile();
		return files;
	}

	public static final File[] resolveDependency(final String qualifiedNamed) {
		return resolveDependency(qualifiedNamed, null);
	}

	public static final File resolveSingleDependency(final String qualifiedNamed) {
		return resolveSingleDependency(qualifiedNamed, null);
	}
}
