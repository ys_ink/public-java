package com.inkcode;

import static org.jboss.arquillian.graphene.Graphene.guardHttp;
import static org.junit.Assert.assertEquals;

import java.net.URL;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.drone.api.annotation.Drone;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

@RunWith(Arquillian.class)
public class IndexTest {
    
    @Deployment
    public static WebArchive createDeployment() {
    	return Deployments.createLoginScreenDeployment();
    }

    @Drone
    private WebDriver browser;
    
    @ArquillianResource
    private URL deploymentUrl;
    
    @FindBy(id="loginForm:username:label")                        // 2. injects an element
    private WebElement userNameLabel;
    
    @FindBy(id="loginForm:username:input")                        // 2. injects an element
    private WebElement userNameInput;
    
    
    @FindBy(id="loginForm:password:label")                        // 2. injects an element
    private WebElement passwordLabel;
    
    
    @FindBy(id="loginForm:password:input")                        // 2. injects an element
    private WebElement passwordInput;
    
    @FindBy(id = "loginForm:login")
    private WebElement loginButton;
    
    @FindBy(tagName = "li")                     
    private WebElement facesMessage;
    
    @Test
    @RunAsClient
    public void showIndexPage() {
    	browser.get(deploymentUrl.toExternalForm() + "index.xhtml");      // 1. open the tested page
    	assertEquals("Username:", userNameLabel.getText());
    }
    
    @Test
    @RunAsClient
    public void loginSuccessful() {
    	browser.get(deploymentUrl.toExternalForm() + "index.xhtml");      // 1. open the tested page
        
    	userNameInput.sendKeys("ink");
    	passwordInput.sendKeys("test");
    	guardHttp(loginButton).click();
    	System.out.println(browser.getPageSource());
    	assertEquals(LoginController.SUCCESS_MESSAGE, facesMessage.getText().trim());
    }
    
    @Test
    @RunAsClient
    public void loginFailed() {
    	browser.get(deploymentUrl.toExternalForm() + "index.xhtml");      // 1. open the tested page
        
    	userNameInput.sendKeys("somedude");
    	passwordInput.sendKeys("wrongpassword");
    	guardHttp(loginButton).click();
    	System.out.println(browser.getPageSource());
    	assertEquals(LoginController.FAILURE_MESSAGE, facesMessage.getText().trim());
    }
    
    
}