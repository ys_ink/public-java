#Functional Tests README

##Getting Started
If you want to start right away, just execute *mvn clean install* on the root artifact *functional-tests-parent*

###functional-tests-parent
###functional-tests-demo-services
This artifact provides the demo service classes. They are automatically injected via CDI into the JSF context.
###functional-tests-demo-web-components
This artifact provides the web components used throughout the demo. They are located in /src/main/resources/META-INF/resources. There are two types of components:

+ Directly inside the *resources* folder are .xhtml Files located, which can be used in the eintire JSF application.
+ Inside *inkcode* are some composite-components located. They can be used via declaring the namespace *http://java.sun.com/jsf/composite/inkcode* inside your xhtml file 

###functional-tests-demo-war
This artifact does only contain files in *src/main/webapp/WEB-INF*. It has dependencies on *demo-web-components* & *demo-services* and is only used to show that the files inside *demo-web-components* is correctly included into the WAR to be used. 
If you want to try it out, just build it and deploy the created WAR archive *inkcode-demo.war* to your favorite application server (e.g. Wildfly). The correct login data is *ink* with *test* as password.
###functional-tests-tests-parent
This parent artifact declared basic stuff needed to execute the functional tests with arquillian, drone etc.
##functional-tests-tests-web-components

	