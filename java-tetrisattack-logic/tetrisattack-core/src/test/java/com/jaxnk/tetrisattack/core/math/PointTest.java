package com.jaxnk.tetrisattack.core.math;

import org.junit.Test;
import static org.junit.Assert.*;

public class PointTest {
	
	@Test
	public void testEquals() {
		Point p1 = new Point(5, 5);
		Point p2 = new Point(5, 5);
		
		assertTrue(p1.equals(p2));
	}
	
	@Test
	public void testHashCode() {
		Point p1 = new Point(5, 5);
		Point p2 = new Point(5, 5);
		
		assertTrue(p1.hashCode() == p2.hashCode());
	}
}
