package com.jaxnk.tetrisattack.core;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class GameTest {
	private GameLogic logic;
	
	@Before
	public void construct() {
		logic = new GameLogic();
	}
	
	@Test
	public void fieldReturnElementNotNull() {
		logic.addGameObject(new GameObjectSimple(), 15, 3);
		
		GameObject o = logic.getGameObject(15, 3);
		assertTrue(o != GameObject.NULL);
	}
	
	@Test
	public void fallingBlockThingStuff() {
		logic.addGameObject(new GameObjectSimple(), 3, 1);
		
		GameObject o = logic.getGameObject(3, 1);
		assertTrue(o != GameObject.NULL);
		
		logic.processLogicStep();
		
		o = logic.getGameObject(3, 0);
		assertTrue(o != GameObject.NULL);
	}
	
	@Test
	public void timebasedLogic() {
		logic.addGameObject(new GameObjectSimple(), 0, 5);
		checkForNull(0, 5);
		
		doLogicTimeBased(System.currentTimeMillis());
		checkForNull(0, 4);
		
		doLogicTimeBased(System.currentTimeMillis());
		checkForNull(0, 3);
		
		doLogicTimeBased(System.currentTimeMillis());
		checkForNull(0, 2);
		
		doLogicTimeBased(System.currentTimeMillis());
		checkForNull(0, 1);
		
		doLogicTimeBased(System.currentTimeMillis());
		checkForNull(0, 0);		
	}

	private void checkForNull(int col, int row) {
		assertTrue(logic.getGameObject(col, row) != GameObject.NULL);
	}

	private void doLogicTimeBased(long timeSince) {
		waitingForNoobs(timeSince);
		logic.processLogicStep();
	}

	private void waitingForNoobs(long timeSince) {
		while(System.currentTimeMillis() <= timeSince + 2000)
		{
			//do nothing!
		}
		System.out.println("waiting done");
	}
}
