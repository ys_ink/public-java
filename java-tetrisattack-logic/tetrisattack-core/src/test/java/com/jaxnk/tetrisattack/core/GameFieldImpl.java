package com.jaxnk.tetrisattack.core;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.jaxnk.tetrisattack.core.math.Point;

public class GameFieldImpl implements GameField {
	private static final int SIZE_WIDTH = 4;
	private static final int SIZE_HEIGHT = 6;
	
	private BiMap<Point, GameObject> gameObjects = HashBiMap.create();
	
	/* (non-Javadoc)
	 * @see com.jaxnk.tetrisattack.core.IGameField#getGameObject(int, int)
	 */
	@Override
	public GameObject getGameObject(int col, int row) {
		Point p = new Point(col, row);
		if(!gameObjects.containsKey(p))
			return GameObject.NULL;
		
		return gameObjects.get(p);
	}

	/* (non-Javadoc)
	 * @see com.jaxnk.tetrisattack.core.IGameField#addGameObject(com.jaxnk.tetrisattack.core.GameObject, int, int)
	 */
	@Override
	public void addGameObject(GameObject gameObject, int col, int row) {
		gameObjects.put(new Point(col, row), gameObject);
	}

	@Override
	public int getWidth() {
		return SIZE_WIDTH;
	}

	@Override
	public int getHeigt() {
		return SIZE_HEIGHT;
	}

	@Override
	public void moveGameObjectDown(GameObject obj) {
		Point pOld = gameObjects.inverse().get(obj);
		Point pNew = new Point(pOld.x, pOld.y - 1);
		
		gameObjects.remove(pOld);
		gameObjects.put(pNew, obj);
	}

}
