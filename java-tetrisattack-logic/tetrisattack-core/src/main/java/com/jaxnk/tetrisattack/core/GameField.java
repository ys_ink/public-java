package com.jaxnk.tetrisattack.core;

public interface GameField {

	public abstract GameObject getGameObject(int col, int row);

	public abstract void addGameObject(GameObject gameObject, int col, int row);

	public abstract int getWidth();

	public abstract int getHeigt();

	public abstract void moveGameObjectDown(GameObject obj);
}