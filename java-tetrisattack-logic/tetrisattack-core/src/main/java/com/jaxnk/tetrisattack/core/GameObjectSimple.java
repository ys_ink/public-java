package com.jaxnk.tetrisattack.core;

public class GameObjectSimple implements GameObject {
	private GameObjectState state;
	
	@Override
	public void setState(GameObjectState state) {
		this.state = state;
	}

	@Override
	public GameObjectState getState() {
		return this.state;
	}
}
