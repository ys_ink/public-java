package com.jaxnk.tetrisattack.core;



public class GameLogic {
	GameField gameField = new GameFieldImpl();
	
	public void processLogicStep() {
		updateGameObjectStates();
		moveFallingGameObjects();
	}

	private void moveFallingGameObjects() {
		int width = gameField.getWidth();
		int height = gameField.getHeigt();
		
		for(int col = 0; col <= width; ++col)
		{
			for(int row = 0; row <= height; ++row)
			{
				GameObject obj = gameField.getGameObject(col, row);
				if(obj.getState() == GameObjectState.FALLING)
				{
					gameField.moveGameObjectDown(obj);
				}
			}
		}
	}

	private void updateGameObjectStates() {
		int width = gameField.getWidth();
		int height = gameField.getHeigt();
		
		for(int col = 0; col <= width; ++col)
		{
			for(int row = 0; row <= height; ++row)
			{
				GameObject obj = gameField.getGameObject(col, row);
				
				if(obj == GameObject.NULL)
				{
					while(row <= height)
					{
						obj = gameField.getGameObject(col, row);
						
						obj.setState(GameObjectState.FALLING);
						
						++row;
					}
					break;
				}
			}
		}
	}

	public GameObject getGameObject(int col, int row) {
		return gameField.getGameObject(col, row);
	}

	public void addGameObject(GameObject gameObject, int col, int row) {
		gameField.addGameObject(gameObject, col, row);
	}
}
