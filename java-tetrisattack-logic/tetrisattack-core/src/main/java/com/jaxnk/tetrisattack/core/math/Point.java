package com.jaxnk.tetrisattack.core.math;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class Point 
{               
    // Members
    public int x;
    public int y;
    
    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public boolean equals(Object other) {
    	if(!(other instanceof Point))
    		return false;
    	Point o = (Point) other;
    	
    	EqualsBuilder b = new EqualsBuilder();
    	return b.append(x, o.x).append(y, o.y).build();
    }
    
    @Override
    public int hashCode() {
    	HashCodeBuilder b = new HashCodeBuilder(3,3939393);
    	return b.append(x)
    	 .append(y).build();
    }
}
